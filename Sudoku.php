<?php

/**
 * The Sudoku Class which has an solving algorithm included using backtracking
 * @author Niclas Simmler <niclas.simmler@gmail.com>
 * @version 1.0 / 2016-01-02
 */
class Sudoku
{

    /**
     * The grid
     * @var array
     */
    private $grid = array();

    /**
     * The backup grid, to be used during solving to determine the preset values
     * @var array
     */
    private $backup = array();

    /**
     * The dimension of the sudoku (tested with 4, 9 - bigger ones should work too)
     * @var int
     */
    private $dimension;

    /**
     * A threshold which is being used during solving to output current progress
     * Increase this if you want less output
     * @var int
     */
    private $stateOutputThreshold = 100000;

    /**
     * An iteration counter to count the steps during solving execution
     * @var int
     */
    private $iterationCounter = 0;

    /**
     * Constructor
     * @param int $dimension
     */
    public function __construct($dimension = 9)
    {
        $this->dimension = $dimension;
        $this->init();
    }

    /**
     * Initializer called by Constructor
     */
    private function init()
    {
        for ($i = 0; $i < $this->dimension; $i++) {
            $row = array();
            for ($j = 0; $j < $this->dimension; $j++) {
                array_push($row, 0);
            }
            array_push($this->grid, $row);
        }
    }

    /**
     * Returns the cell value at a given coordinate
     * @param $x The Column
     * @param $y The Row
     * @return mixed
     */
    public function getCellValue($x, $y)
    {
        return $this->grid[$y][$x];
    }

    /**
     * Sets a cell value to a given value
     * @param $x The Column
     * @param $y The Row
     * @param $value The Value to be set
     */
    public function setCellValue($x, $y, $value)
    {
        $this->grid[$y][$x] = $value;
    }

    /**
     * Get the grid
     * @return array
     */
    public function getGridRaw()
    {
        return $this->grid;
    }

    /**
     * Gets the n-th Row of the grid
     * @param $n The Row Number
     * @return mixed
     */
    private function getRow($n)
    {
        return $this->getGridRaw()[$n];
    }

    /**
     * Gets the n-th Column of the grid
     * @param $n The Column Number
     * @return array
     */
    private function getColumn($n)
    {
        return array_column($this->grid, $n);
    }

    /**
     * Computes the Box Number for a given Cell
     * BOX NUMBERING FOR A 9x9 SUDOKU
     * 1 2 3
     * 4 5 6
     * 7 8 9
     * BOX NUMBERING FOR A 4x4 SUDOKU
     * 1 2
     * 3 4
     *
     * @param $x The Column
     * @param $y The Row
     * @return int The Box Number
     */
    private function getBoxNumber($x, $y)
    {
        $boxRow = (int)($y / sqrt($this->dimension));
        $boxCol = (int)($x / sqrt($this->dimension));
        return $boxRow * sqrt($this->dimension) + $boxCol + 1;
    }

    /**
     * Pretty Print function
     */
    public function printGridPretty()
    {
        printf("---------------------\n");
        $remainder = sqrt($this->dimension) - 1;
        $headLine = "*|";
        for ($h = 0; $h < $this->dimension; $h++) {
            $headLine .= $h;
            if (($h % sqrt($this->dimension)) - $remainder == 0) {
                $headLine .= "|";
            } else {
                $headLine .= " ";
            }
        }
        printf($headLine . "x\n");
        $rowSeperator = "-+";
        for ($s = 0; $s < $this->dimension; $s++) {
            $rowSeperator .= "-";
            if (($s % sqrt($this->dimension)) - $remainder == 0) {
                $rowSeperator .= "+";
            } else {
                $rowSeperator .= "-";
            }
        }
        for ($i = 0; $i < $this->dimension; $i++) {
            if ($i % sqrt($this->dimension) == 0) {
                printf($rowSeperator . "\n");
            }
            $row = $this->getRow($i);
            $printer = $i . "|";
            for ($j = 0; $j < $this->dimension; $j++) {
                $printer .= $row[$j];
                if (($j % sqrt($this->dimension)) - $remainder == 0) {
                    $printer .= "|";
                } else {
                    $printer .= " ";
                }
            }
            printf("%s \n", $printer);
        }
        printf($rowSeperator . "\n");
        printf("y\n");
    }

    /**
     * Loader Function to load a preset sudoku which will then later be solved
     * @param $data A Sudoku consisting of an two dimensional array
     * @return bool
     */
    public function load($data)
    {
        $passing = false;
        if (count($data) == $this->dimension) {
            foreach ($data as $row) {
                $passing = (count($row) == $this->dimension);
            }
        }

        if ($passing) {
            $this->grid = $data;
            return true;
        }
        return false;
    }

    /**
     * The Public solver interface
     * @return bool
     */
    public function solve()
    {
        $this->backup = $this->grid;
        try {
            $this->solver(0, 0);
        } catch (Exception $e) {
            printf("%s after %d Steps\n", $e->getMessage(), $this->iterationCounter);
            return true;
        } finally {
            return false;
        }
    }

    /**
     * The algorithm, can and should not be called directly
     * @param $x The Column
     * @param $y The Row
     * @return bool
     * @throws Exception when done
     */
    private function solver($x, $y)
    {
        $this->iterationCounter++;
        if ($this->iterationCounter % $this->stateOutputThreshold == 0) {
            print("Current State @ Step " . $this->iterationCounter . "\n");
            $this->printGridPretty();
            print("----\n");
        }
        if ($x == $this->dimension) {
            $x = 0;
            $y++;
            if ($y == $this->dimension) {
                throw new Exception("Done");
            }
        }
        if (!$this->isEmptyCell($x, $y)) {
            $this->solver($x + 1, $y);
        } else {
            for ($i = 1; $i <= $this->dimension; $i++) {
                if ($this->isValid($x, $y, $i)) {
                    $this->setCellValue($x, $y, $i);
                    if ($this->solver($x + 1, $y)) {
                        return true;
                    }

                }
            }
        }
        $this->setNull($x, $y);
        return false;
    }

    /**
     * Checks if a cell is empty at a given coordinate
     * @param $x The Column
     * @param $y The Row
     * @return bool
     */
    private function isEmptyCell($x, $y)
    {
        return ($this->getCellValue($x, $y) == 0);
    }

    /**
     * Sets a cell to null if it is nullable
     * @param $x The Column
     * @param $y The Row
     */
    private function setNull($x, $y)
    {
        if ($this->_isNullable($x, $y)) {
            $this->setCellValue($x, $y, 0);
        }
    }

    /**
     * Checks if a cell is nullable (if it is not a preset cell)
     * @param $x The Column
     * @param $y The Row
     * @return bool
     */
    private function _isNullable($x, $y)
    {
        return ($this->backup[$y][$x] == 0);
    }

    /**
     * Checks if a given value can be written to cell by checking the constraints
     * @param $x The Column
     * @param $y The Row
     * @param $value The Value to be set
     * @return bool
     */
    private function isValid($x, $y, $value)
    {
        return ($this->_isValidInRow($y, $value)
            && $this->_isValidInColumn($x, $value)
            && $this->_isValidInBox($this->getBoxNumber($x, $y), $value));
    }

    /**
     * The Row Constraint
     * @param $row_n The Row Number
     * @param $value The Value
     * @return bool
     */
    private function _isValidInRow($row_n, $value)
    {
        $row = $this->getRow($row_n);
        $isValid = false;
        for ($i = 0; $i < $this->dimension; $i++) {
            $isValid = ($row[$i] != $value);
            if (!$isValid) {
                return false;
            }
        }
        return $isValid;
    }

    /**
     * The Column constraint
     * @param $col_n The Column Number
     * @param $value The Value
     * @return bool
     */
    private function _isValidInColumn($col_n, $value)
    {
        $column = $this->getColumn($col_n);
        $isValid = false;
        for ($i = 0; $i < $this->dimension; $i++) {
            $isValid = ($column[$i] != $value);
            if (!$isValid) {
                return false;
            }
        }
        return $isValid;
    }

    /**
     * The Box constraint
     * @param $box_n The Box Number
     * @param $value The Value
     * @return bool
     */
    private function _isValidInBox($box_n, $value)
    {
        $isValid = false;
        $colRemainder = ($box_n - 1) % sqrt($this->dimension);
        $bcStart = floor($colRemainder) * sqrt($this->dimension);

        $rowRemainder = ($box_n - 1) / sqrt($this->dimension);
        $brStart = floor($rowRemainder) * sqrt($this->dimension);

        $bcEnd = $bcStart + sqrt($this->dimension);
        $brEnd = $brStart + sqrt($this->dimension);

        for ($i = $bcStart; $i < $bcEnd; $i++) {
            for ($j = $brStart; $j < $brEnd; $j++) {
                $isValid = ($this->grid[$j][$i] != $value);
                if (!$isValid) {
                    return false;
                }
            }
        }
        return $isValid;
    }
}
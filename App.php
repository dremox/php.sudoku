<?php

	ini_set('memory_limit','2048M');
	require 'Sudoku.php';

	$sudoku = new Sudoku();

    //a hard sudoku grid with only 17 clues http://theconversation.com/good-at-sudoku-heres-some-youll-never-complete-5234
    //takes roughly 34800000+ Steps
	$grid = array(
		array(0,0,0,7,0,0,0,0,0),
		array(1,0,0,0,0,0,0,0,0),
		array(0,0,0,4,3,0,2,0,0),
		array(0,0,0,0,0,0,0,0,6),
		array(0,0,0,5,0,9,0,0,0),
		array(0,0,0,0,0,0,4,1,8),
		array(0,0,0,0,8,1,0,0,0),
		array(0,0,2,0,0,0,0,5,0),
		array(0,4,0,0,0,0,3,0,0)
	);

    //an easy sudoku with a lot of clues http://www.puzzles.ca/sudoku_puzzles/sudoku_easy_231_solution.html
	$grid2 = array(
		array(0,4,0,0,9,0,0,0,0),
		array(0,7,0,0,1,8,9,0,0),
		array(0,9,0,0,0,6,1,0,0),
		array(0,0,0,0,0,0,3,0,0),
		array(5,2,6,8,0,0,0,0,4),
		array(0,0,4,6,0,0,0,5,8),
		array(3,0,0,0,4,0,0,0,2),
		array(0,6,0,0,0,0,0,0,0),
		array(1,0,0,2,0,5,0,0,0)
	);

	$sudoku->load($grid2);

	$sudoku->solve();

	$sudoku->printGridPretty();

